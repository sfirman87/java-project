FROM openjdk:11.0.13-jre
WORKDIR /app/java
COPY . .
CMD [ "java", "-jar", "target/test-java-1.0-SNAPSHOT.jar"]
